<p align="center"> <a href="https://linktr.ee/meghdeb" target="_blank"> <img src="https://i.pinimg.com/originals/8b/c8/13/8bc8138470ece0f8c5a6dc3cd715de92.png" alt="banner"/> </a>
<h1 align="center">Hi 👋, I'm Megh Deb</h1>
<h3 align="center">A passionate entrepreneur and programmer from Kolkata</h3>

<p align="left"> <a href="https://github.com/ryo-ma/github-profile-trophy"><img src="https://github-profile-trophy.vercel.app/?username=Megh2005&theme=matrix&no-bg=true" alt="Megh2005" /></a> </p>

- 🔭 I’m currently working on **[Heritage Institute of Technology](https://heritageit.edu)**

- 🌱 I’m currently learning **[Kotlin](https://kotlinlang.org/)**

- 👯Connect me through my **[Social Links](https://linktr.ee/meghdeb)**

- 🤝 I’m also available at **[Google for Developers](https://g.dev/MeghDeb)**

- 👨‍💻 All of my projects are available at **[GitHub](https://github.com/Megh2005)**

- 📝 I write articles on **[Designing](https://www.linkedin.com/posts/megh-deb-20637a2a1_connections-comments-activity-7169906081660436480-_bpa?utm_source=share&utm_medium=member_desktop)**

- 💬 Ask me about **Marketing , Android**

- 📫 How to reach me **[iammeghdeb@gmail.com](mailto:iammeghdeb@gmail.com?subject=I%20want%20to%20connect)**

- 📄 Know about my experiences **[click here](https://old-portfolio-dj58tt4aw-megh-debs-projects.vercel.app/)**



<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a> <a href="https://www.figma.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/> </a> <a href="https://cloud.google.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/google_cloud/google_cloud-icon.svg" alt="gcp" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://kotlinlang.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-icon.svg" alt="kotlin" width="40" height="40"/> </a> <a href="https://www.linux.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> </a> <a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> <a href="https://reactjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> <a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-original.svg" alt="typescript" width="40" height="40"/> </a> </p>



<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=Megh2005&show_icons=true&theme=dark&locale=en" alt="Megh2005" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=Megh2005&theme=highcontrast" alt="Megh2005" /></p>
<a href="https://www.buymeacoffee.com/iammeghdeb" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>
